using System;

namespace Lab2
{
    public class InfaBid
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public decimal Coefficient { get; set; }

        public float Price { get; set; }

        public override string ToString()

        {

            return string.Format("Bid Id:{0} Name:{1} Category:{2} Price:{3}$", Id, Name, Coefficient, Price);

        }

        public InfaBid(int id, string name, decimal coefficient, float price)
        {
            Id = id;
            Name = name;
            Coefficient = coefficient;
            Price = price;
        }
    }
}
