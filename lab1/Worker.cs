﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab1
{
    public class Worker : Human
    {
        public string department { get; set; }
        public string post { get; set; }

        public override string ToString()
        {
            return base.ToString() + " " + department + " " + post;
        }
    }
}
