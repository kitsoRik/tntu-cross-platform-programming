﻿using System;

namespace lab1
{

    class Program
    {

        static void Main(string[] args)
        {
            var student = new Student()
            { 
                age = 21, 
                birthday = new DateTime(2000, 8, 16), 
                gender = Gender.Male, 
                nationality = Nationality.Ukrainian, 
                Name = "Rostyslav Pidburachynskyi", 
                group = "SNs-42",
                numberBook = 46200,
                pasportNumber = 000123123,
                height = 184,
                mass = 80
            };
            Console.WriteLine(student);
        }
    }
}
