﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab1
{
    public enum Gender
    {
        Male, Female
    }

    public enum Nationality
    {
        Russian, Ukrainian, British, American, Jewish
    }

    public enum Religion
    {
        Christianity, Orthodoxy, Islam, Buddhism
    }

    public interface IHuman
    {
        int age { get; }
        Gender gender { get; }
        int height { get; }
        int mass { get; }
        Nationality nationality { get; }
        DateTime birthday {get;}
        int pasportNumber { get; }
    }

    
}
