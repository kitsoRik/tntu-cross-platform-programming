﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab1
{
    public class Student : Human
    {
        public int numberBook { get; set; }
        public string group { get; set; }

        public override string ToString()
        {
            return base.ToString () + " " + numberBook + " " + group;
        }
    }
}
