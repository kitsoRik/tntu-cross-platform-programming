﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab1
{
    public class Human : IHuman
    {
        public string Name { get; set; }
        public int age { get; set; }

        public Gender gender { get; set; }

        public int height { get; set; }

        public int mass { get; set; }

        public Nationality nationality { get; set; }

        public DateTime birthday { get; set; }

        public int pasportNumber { get; set; }

        public override string ToString()
        {
            return Name +" "+ age + " years " + gender + " " + height + " m " + mass + " " + nationality + " " + birthday + " " + pasportNumber;
        }

    }

}
